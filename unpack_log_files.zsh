#!/usr/bin/zsh -x

SEPARATOR='1094/'
RSE='DESY-HH_LOCALGROUPDISK'

function find_and_unpack {
    tarballs=($(find -type f -name '*.tgz'))
    for ball in $tarballs; do
        tar --directory $(dirname $ball) -xaf $ball
        echo "unpacking $ball"
        rm $ball
    done

    if [[ -n ${tarballs[@]} ]]; then
        return 1
    fi

    return 0
}

function unpack_whole_did {
    did=$1
    mkdir --parents $did
    cd $did
    logfiles=($(rucio list-file-replicas --pfns --rses $RSE $did | awk -F $SEPARATOR '{print $2}'))
    for f in $logfiles; do
        tar -xaf $f
    done
    until find_and_unpack; do
        # nothing
    done
    
    cd $OLDPWD
}

for did in $@; do
    unpack_whole_did $did
done
