Tool to check the status of grid production jobs and then replicate the output datasets of finished jobs.

A second shell script can then unpack log file containers

Installation
------------
Simply clone this repo, all dependencies are on `/cvmfs`:
```
git clone ssh://git@gitlab.cern.ch:7999/jneundor/production_task_replicator.git
```

Usage
-----
This script requires the PanDA and Rucio python clients, which are currently only available for python2.
Also, a valid grid proxy is needed to interface with rucio.
Therefore, you need to execute
```
setupATLAS -3
lsetup panda
lsetup rucio
rucio voms-proxy-init -voms atlas
```
before using this program. The `-h` flag shows the options:
```
 % ./replicator.py -h
 usage: Tool to replicate the output containers of PanDA jobs
       [-h] -f TASKIDFILE [-u USER] [--scope SCOPE]

optional arguments:
  -h, --help            show this help message and exit
  -f TASKIDFILE, --taskidfile TASKIDFILE
                        file with one task ID per line
  -u USER, --user USER  PanDA account name of the task owner
  --scope SCOPE         rucio scope. If not given, try to guess from DID name
```
The only non-straightforward option is probably `--user`: The PanDA username is not the regular CERN username, but typically the full name of the submitter. 
It can be seen on the task information page in https://bigpanda.cern.ch/task.

Note that this script does **not** check whether there is already a replication rule for the given DID on the given RSE.
The rucio client throws an exception if that is the case, at least on `DESY-HH_LOCALGROUPDISK`.
If you start using this script at a new site or RSE, you should run it twice in close succession and check with `rucio list-rules` whether it creates duplicate rules.

Log File Unpacking
------------------

Just run 
```
./unpack_log_files.zsh DID [DID ...]
```
in a folder. Requires rucio to be available at a valid proxy
