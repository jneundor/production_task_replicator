#!/usr/bin/env python3

from __future__ import print_function, division

import argparse
import sys

from pandaclient import panda_api
from rucio.client.ruleclient import RuleClient
import rucio.common.exception

def make_scope_name_dict(did, scope=None):
    '''try to guess scope if not given'''
    ret = {'name':did}
    if scope is None:
        parts = did.split('.')
        scope = '.'.join(parts[:2])
    ret['scope'] = scope
    return ret
    
def main(args):

    task_status = ['done']
    if args.replicate_finished:
        task_status.append('finished')

    pandaapi = panda_api.get_api()

    with open(args.taskidfile) as tidf:
        taskids = [line.strip()for line in tidf.readlines() if not line.strip().startswith('#')]

    tasks_all = pandaapi.get_tasks(taskids, username=args.user)

    # proceed only with finished tasks
    tasks = [task for task in tasks_all if task['status'] in task_status]
    # each task has a list of datasets
    # we only need the ones marked 'output'
    # and from those, we need only the container name
    # since container names may be duplicate, the result is stored in a set
    datasets = {ds['containername'].strip('/') for task in tasks for ds in task['datasets'] if ds['type'] == 'output'}

    # the rucio client has two issues:
    # first, it needs to be given a scope
    # second, it cannot handle sets, it really needs a list
    full_did_dicts = [make_scope_name_dict(did, args.scope) for did in datasets]

    try:
        rucioclient = RuleClient()
    except rucio.common.exception.CannotAuthenticate:
        print('could not authenticate to rucio -- run `voms-proxy-init -voms atlas`!')
        sys.exit(1)


    # rules may actually already exist. If there already is a rule for one did in the list,
    # the client will throw an exception
    # therefore, loop over them individually
    for did_dict in full_did_dicts:
        try:
            rucioclient.add_replication_rule([did_dict], 1, args.rse)
            print('created rule for {}:{}'.format(did_dict['scope'], did_dict['name']))
        except rucio.common.exception.DuplicateRule:
            pass

    if args.print_incomplete:
        incomplete_tasks = [task for task in tasks_all if task['status'] != 'done']
        for task in incomplete_tasks:
            print('task {tid} is in status {status}'.format(
                tid=task['jeditaskid'],
                status=task['status']
            ))

if __name__ == '__main__':

    parser = argparse.ArgumentParser('Tool to replicate the output containers of PanDA jobs')
    parser.add_argument('-f', '--taskidfile', help='file with one task ID per line', required=True)
    parser.add_argument('-u', '--user', help='PanDA account name of the task owner. Can be omitted for your own tasks.', default=None)
    parser.add_argument('--rse', help='Rucio Storage Element', required=True)
    parser.add_argument('--scope', help='rucio scope. If not given, try to guess from DID name', default=None)
    parser.add_argument('--print-incomplete', help='print incomplete task IDs', action='store_true')
    parser.add_argument('--replicate-finished', help='treat status "finished" the same as "done"', action='store_true')

    arguments = parser.parse_args()

    main(arguments)
